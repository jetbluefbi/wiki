.. wiki documentation master file, created by
   sphinx-quickstart on Mon Oct  8 09:36:44 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

JetBlue Finance Business Intelligence Wiki
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   project-management.rst

Introduction
============

This is an introduction to the project I am working on.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
