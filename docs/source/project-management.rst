==================
Project Management
==================

This is a file that will summarize the steps you need to use for project
management. It should act as a reference for all that you do.

Setting up a project
====================

This is the process you go through to set up a project.